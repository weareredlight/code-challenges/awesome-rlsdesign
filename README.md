# Awesome Design Challenge

The Design Challenge consists of redesigning and implementing the following three screens of RedLight's internal management tool. There are no restrictions regarding grid, layout, navigation, content organization, fonts or color palette.


> *Screen:* Profile
>
> *Purpose:* View that employee info: personal details, project allocations, vacations, skills, pending actions, gadgets, etc

![Profile](/public/profile.png?raw=true)

> *Screen:* Project Allocations
>
> *Purpose:* Top level view of all employees and their project allocation on a certain timeframe

![Project Allocations](/public/project-allocations.png?raw=true)

> *Screen:* New Project Allocation
> 
> *Purpose:* Allocate a project to an employee during a timeframe (an employee can be allocated to multiple projects simultaneously)

![New Project Allocation](/public/new-project-allocation.png?raw=true)

### Tools

At RedLight we mainly use Sketch and Figma but you can work with your favorite tool.

### Palette

You are free to create your own color palette for this challenge but for reference, RedLight's site has the following color palette:

`#e21339` red

`#392f94` blue

`#df9234` mustard

`#000000` black

`#ffffff` white

### Fonts

RedLight's official fonts are `Roobert` and `Space Mono`. However you can choose whichever you want.

### Deliverables

**Responsive mockups:**
- Mobile (375px width)
- Desktop (1440px width)

**Screens:**
- Profile
- Project Allocations
- New Project Allocation

**Code:**
- HTML
- SASS/LESS

**Other**

Feel free to add anything else you see fit to justify your line of thought and decisions, e.g. a file `process.txt` or a companion image.
